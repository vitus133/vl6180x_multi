# __Multiple VL6180X sensors on the same I2C bus__

# Introduction

This module allows operating several VL6180X Time Of Flight sensors on the same I2C bus. This is achieved by reallocating each sensor' address.
The project is intended to run on RaspberryPi. Specifically, it was tested on RaspberryPi 4 with Python 3.7

# Prerequisites
The example below is using Raspberry PI default I2C bus and two GPIOs to control two sensors:

<img src="images/connections.png">

# Running the example
1. Clone the repository:
```bash
git clone https://gitlab.com/vitus133/vl6180x_multi && cd vl6180x_multi
```
2. Create and activate Python virtual environment, install dependencies:
```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

```
3. Install the package
```bash
pip install -e .
```
4. Run the example
```bash
cd example/
python range.py
```

# Installing from PyPi
```bash
pip install vl6180x-multi
```
Latest Raspberry PI OS releases configure pip to download binary builds from https://www.piwheels.org/
The wheels are built automatically for pypi packages.
If something in the process of installing from piwheels.org is not working for you, I suggest source install from PyPi as a workaround:
1. Comment out this line in `/etc/pip.conf`:
```
extra-index-url=https://www.piwheels.org/simple
```
2. Install Python headers:
```bash
sudo apt install python3-dev
```
3. Re-run 
```bash
pip install vl6180x-multi
```



